# Website Key Detection

Type in the secret code (just start typing it in while in the live demo) and get a surpise. Secret code can be found in index.html

## Live Demo
[https://projects.gregoryhammond.ca/website-key-detection/](https://projects.gregoryhammond.ca/website-key-detection/)

## License
This project is under Unlicense or Beerware or WTFPL or GNU General Public License v3.0 (GPL-3.0+) or MIT or Apache License 2.0. You can choose to use any of these licenses, credit is appreciated but not required.